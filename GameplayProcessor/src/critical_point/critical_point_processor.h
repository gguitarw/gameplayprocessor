﻿#pragma once
#include <cstdint>
#include <memory>
#include <vector>
#include "point/point.h"
#include "beatmap.h""

using spud_base::point::Point;
using beatmap_parser::Beatmap;

namespace gameplay_processor {
    namespace critical_point { // could probably just be crit_point
        enum CriticalPointType {
            kCritCircle = 1,
            kCritSlider = 2,
            kCritSliderStart = 4,
            kCritSliderTick = 8,
            kCritSliderEnd = 16,
            kCritSpinner = 32,
            kCritSpinnerStart = 64,
            kCritSpinnerEnd = 128
        };

        struct CriticalPoint {
            Point<int> coord;
            int64_t time;
            CriticalPointType type;

            CriticalPoint() : coord{-1,-1}, time{-1}, type() {}
            CriticalPoint(Point<int> coord, int64_t time, CriticalPointType type) :
                coord{ coord }, time{ time }, type{ type } {}

            // used for binary search (like lower_bound())
            bool operator<(const CriticalPoint& p) const {
                return time < p.time;
            }
        };

        class CriticalPointProcessor {
        public:
            std::vector<CriticalPoint> crit_points;
            
            // TODO: Load(beatmap);
            void Load(const std::string& beatmap_path);
            void Load(Beatmap beatmap);
            void ProcessCritPoints();
            void WriteToFile(std::string output_folder = "default") const;
        private:
            // std::unique_ptr<Beatmap> beatmap;
            Beatmap beatmap_;
        };
    };
}
