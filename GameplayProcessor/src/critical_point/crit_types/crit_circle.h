﻿#pragma once
#include "crit_object.h"

namespace gameplay_processor {
    namespace critical_point {
        class CritCircle : public CritObject {
        public:
            inline explicit CritCircle(const HitObject& hit_object) : CritObject(hit_object) {};

            void GenerateCritPoints() override {
                // add the circle itself as the crit point
                crit_points.push_back({ hit_object.coord, hit_object.timing, kCritCircle });
            };
        };
    }
}
