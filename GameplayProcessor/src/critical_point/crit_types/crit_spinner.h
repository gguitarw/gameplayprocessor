﻿#pragma once
#include "crit_object.h"

namespace gameplay_processor {
    namespace critical_point {
        class CritSpinner : public CritObject {
        public:
            inline explicit CritSpinner(const HitObject& hit_object) : CritObject(hit_object) {};

            void GenerateCritPoints() override {
                // add spinner start
                crit_points.push_back({ hit_object.coord, hit_object.timing,
                                      CriticalPointType(kCritSpinner | kCritSpinnerStart) });

                // add spinner end
                crit_points.push_back({ hit_object.coord, hit_object.timing,
                                      CriticalPointType(kCritSpinner | kCritSpinnerEnd) });
            };
        };
    }
}
