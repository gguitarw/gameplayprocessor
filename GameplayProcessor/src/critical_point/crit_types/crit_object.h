﻿#pragma once
#include <vector>
#include "critical_point/critical_point_processor.h"

using beatmap_parser::HitObject;

namespace gameplay_processor {
    namespace critical_point {
        class CritObject {
        public:
            std::vector<CriticalPoint> crit_points{};

            CritObject(const HitObject& hit_object) : hit_object(hit_object) {};

            virtual ~CritObject() = default;

            // implemented by CritCircle, CritSlider, etc...
            virtual void GenerateCritPoints() = 0;
        protected:
            HitObject hit_object;
        };
    }
}

// TODO: implement a derived class for mania objects (click notes and hold notes)