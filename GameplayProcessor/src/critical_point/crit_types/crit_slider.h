﻿#pragma once
#include "crit_object.h"

namespace gameplay_processor {
    namespace critical_point {
        class CritSlider : public CritObject {
        public:
            explicit CritSlider(const HitObject& hit_object, const double tick_rate) : CritObject(hit_object), tick_rate_(tick_rate) {};
            void GenerateCritPoints() override;
        private:
            const double tick_rate_;
            // stores points for slider start, ticks, and end
            std::vector<CriticalPoint> slider_crit_points_{};
            // a point for every ms along the path of the slider
            std::vector<Point<double>> slider_path_points_{};

            void AddSliderStart();
            void AddSliderPath();
            void CalculateSliderTicks();

            // TODO: function for adjusting path so it matches the correct length
            // https://osu.ppy.sh/help/wiki/osu!_File_Formats/Osu_(file_format)
            //      under hit object -> sliders -> length and duration

            void AddRepeatedTicks();

            void CalculateLinearSlider();
            void CalculatePerfectSlider();
            void CalculateBezierSlider();
            // void calculateCatmullSlider();
        };
    }
}
