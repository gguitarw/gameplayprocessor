﻿#include "critical_point_processor.h"

void gameplay_processor::critical_point::CriticalPointProcessor::Load(const std::string& beatmap_path) {
    beatmap_ = {beatmap_path};
}

// TODO: do something instead of copy-constructing beatmap_
void gameplay_processor::critical_point::CriticalPointProcessor::Load(const Beatmap beatmap) {
    beatmap_ = beatmap;
}

void gameplay_processor::critical_point::CriticalPointProcessor::ProcessCritPoints() {
    for (const auto& hitobj : beatmap_.hit_objects) { }
}

void gameplay_processor::critical_point::CriticalPointProcessor::WriteToFile(std::string output_folder) const {
    if (output_folder == "default") {
        output_folder = ""; // make destination the current folder
    }

    // .ocp for osu! critical points
    std::string output_name = beatmap_.information.artist + " - "
                              + beatmap_.information.title + " ["
                              + beatmap_.information.difficulty + "].ocp";

    std::ofstream output_file(output_folder + output_name, std::ios::out);

    for (const auto& point : crit_points) {
        std::string line{ // TODO: better way to do this
            std::to_string(point.time + ','
             + point.coord.x + ',' + point.coord.y + ','
             + point.type + '\n')
        }; // formatted as: time,x,y,type\n
        output_file.write(line.c_str(), line.size());
    }
}
