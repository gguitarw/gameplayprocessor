﻿#pragma once

#include <vector>
#include "point/point.h"
#include "beatmap.h"

using spud_base::point::Point;
using beatmap_parser::HitObject;

namespace gameplay_processor {
    namespace critical_point {
        namespace slider_type {
            struct CirclePoints {
                // three points used to create the circle
                Point<double> point_a;
                Point<double> point_b;
                Point<double> point_c; // TODO: organize this into a struct
            };

            class PerfectSlider {
            public:
                std::vector<Point<double>> path_points;

                PerfectSlider(const HitObject& slider);

            private:
                const HitObject& slider_;

                double length_;

                CirclePoints circle_; 

                void EstimateLength(); // stores result in length_
                void FixLength(); // adjusts points of circle to make length_ equal to the slider_'s length
                void CalculatePath();
            };
        }
    }
}
