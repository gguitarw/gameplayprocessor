﻿#pragma once

#include <vector>
#include "point/point.h"
#include "beatmap.h"

using spud_base::point::Point;
using beatmap_parser::HitObject;

namespace gameplay_processor {
    namespace critical_point {
        namespace slider_type {
            struct BezierCurve {
                std::vector<Point<double>> control_points;
                std::vector<Point<double>> curve_path_points;
                double Length = 0;

                // splits the curve into segments
                std::vector<Point<double>> SegmentCurve(const int segments);

                static Point<double> FindBezierPoint(std::vector<Point<double>>& points, double time);
            private:
                static std::vector<Point<double>> BezierRecurse(std::vector<Point<double>>& points, double time);
            };

            class BezierSlider {
            public:
                BezierSlider(const HitObject& slider);
                std::vector<Point<double>> path_points; // all curves combined
            private:
                const HitObject slider_;
                std::vector<BezierCurve> curves_{};
                double total_length_ = 0;

                void CalculateLengths();
                void FindCurves(const HitObject& slider);
                void GenerateCurvePaths();
            };
        }
    }
}
